<?php
require_once 'class_PDO.php';

date_default_timezone_set('Asia/Manila');

class user extends myDB{

public function register() : void {

    $fullname = htmlentities($_POST['fullname']);
    $username = htmlentities($_POST['username']);
    $password = htmlentities($_POST['password']);

	$sql = "INSERT INTO employee (fullname,username,password) 
			VALUES (?,?,?)";
	
	$this->connectDB();
	$reg_res=$this->execQuery($sql,array($fullname,$username,$password));
	$this->closeDB();
}

public function queryTable($date) : void {
    $sql = "SELECT employee.fullname, time_log.login, time_log.logout, time_log.in_time, time_log.out_time
            FROM employee
            INNER JOIN time_log
            ON employee.emp_id = time_log.emp_id
            WHERE time_log.curr_date = ?";

    $this->connectDB();
    $table_data = $this->queQuery($sql,array($date));
    $this->closeDB();

    if($table_data){
 
        echo '
            <table class="da-table">
                <tr>
                    <th>NAME</th>
                    <th>LOGIN</th>
                    <th>LOGOUT</th>
                </tr>
            ';

        $i = 1;
        foreach($table_data as $row){
            if($i % 2 == 0){
                echo '
                <tr class="stripped">    
                    <td>'.$row['fullname'].'</td>
                    <td>'.$row['login'].' '.$row['in_time'].'</td>
                    <td>'.$row['logout'].' '.$row['out_time'].'</td>
                </tr>
                ';
            }else{
                echo '
                <tr>    
                    <td>'.$row['fullname'].'</td>
                    <td>'.$row['login'].' '.$row['in_time'].'</td>
                    <td>'.$row['logout'].' '.$row['out_time'].'</td>
                </tr>
                ';
            }
           $i++;

        }
        echo '</table>
        <!-- <input style="margin-top:32px;width:180px;float:right;" type="button" name="print_btn" id="print_btn" value="Print">-->' 
        ;
    }else{
        echo '
            <table class="da-table">
                <tr>
                    <th>NAME</th>
                    <th>LOGIN</th>
                    <th>LOGOUT</th>
                </tr>
            ';

        for ($x = 1; $x <= 5; $x++) {
            if($x % 2 == 0){
                echo '
                <tr class="stripped">    
                    <td> </td>
                    <td> </td>
                    <td> </td>
                </tr>
                ';
            }else{
                echo '
                <tr>    
                    <td> </td>
                    <td> </td>
                    <td> </td>
                </tr>
                ';
            }
        }   
        echo '</table>
        <!-- <input style="margin-top:32px;width:180px;float:right;" type="button" name="print_btn" id="print_btn" value="Print">-->' 
        ;
    }
}

public function login() : void {

    $username = htmlentities($_POST['username']);
    $password = htmlentities($_POST['password']);
    
    //Check if user exists

    $sql = "SELECT * FROM employee WHERE username = ? AND password = ?";
    
    $this->connectDB();
    $login_res1 = $this->queQuery($sql,array($username,$password));
    $this->closeDB();

    if($login_res1){
        $login_res1  =  $login_res1[0];
        $emp_id = $login_res1['emp_id'];
        $fname = $login_res1['fullname'];
        $log = date('h:i:s');
        $time = date('A');
        $curr_date = date('Y-m-d');
        
        //echo "ID: ".$emp_id."<br>Fullname: ".$fname."<br>Login: ".$login."<br>Daytime: ".$in_time."<br>Current Date: ".$curr_date;

        //check if user already login/logout.
        $sql2 = "SELECT login, logout FROM time_log WHERE emp_id = ? AND curr_date = ?";
        $this->connectDB();
        $login_res2 = $this->queQuery($sql2,array($emp_id,$curr_date));
        $this->closeDB();

        $login_res2  =  isset($login_res2[0]);
        if(!$login_res2['login'] && isset($_POST['login'])){
            $sql3 = "INSERT INTO time_log (emp_id,login,in_time,curr_date) VALUES (?,?,?,?)";

            $this->connectDB();
            $login_res3=$this->execQuery($sql3,array($emp_id,$log,$time,$curr_date));
            $this->closeDB();
    
            if($login_res3){
                echo '<p class="results">Welcome '.$fname.'! Have a good day!</p>';
            }else{
                echo '<p class="results">Data insertion error! Please contact administrator</p>';
            }
        }else if(!$login_res2['logout'] && isset($_POST['logout'])){
            $sql3 = "UPDATE time_log SET logout = ?, out_time = ? WHERE time_log.emp_id = ? AND time_log.curr_date = ?";
            $this->connectDB();
            $login_res3=$this->execQuery($sql3,array($log,$time,$emp_id,$curr_date));
            $this->closeDB();
        }else if($login_res2['login'] && isset($_POST['login'])){
            echo '<p class="results">You have already login!</p>';
        }else if($login_res2['logout'] && isset($_POST['logout'])){
            echo '<p class="results">You have already logout!</p>';
        }else{
            echo '<p class="results">Attempt Failed. Contact Administrator.</p>';
        }
    }else{
        echo '<p class="results">Login Failed. Either user or password is incorrect.</p>';
    }
}
}


$userObject = new user();
$curr_date = date('Y-m-d');

switch(true)
{
    case isset($_POST['registration']):
        $userObject->register();
        $userObject->queryTable($curr_date);
    break;
    case isset($_POST['login']):
        $userObject->login();
        $userObject->queryTable($curr_date);
    break;
    case isset($_POST['logout']):
        $res = $userObject->login();
        $userObject->queryTable($curr_date);
    break;
    case isset($_POST['curr_date']):
        $queryDate = $_POST['curr_date'];
        $userObject->queryTable($queryDate);
    break;
}



