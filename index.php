<?php require_once 'controller.php'; ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap-grid.css">
    <link rel="stylesheet" href="css/jquery-ui.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/style.css">

    <title>Employee Login System</title>
  </head>
  <body>

    <!-- Registration Form -->
    <div class="da-reg-cont hide">
        <form class="da-reg-form center">
            <h1>New Registration</h1>
            <input type="text" name="emp_name" id="emp_name" placeholder="Full Name">
            <input type="text" name="emp_username" id="emp_username" placeholder="Username">
            <input type="password" name="emp_password" id="emp_password" placeholder="Password">
            <input type="button" name="emp_register" id="emp_register" value="Register">
            <p style="text-align:center;margin-bottom:0;"><a id="reg-close" class="da-reg">Close</a></p>
        </form>
    </div>
    
    <div class="container-fluid">
        <div class="row">
            <!-- Sidebar -->
            <div class="col-4 da-sidebar">
                <img src="img/dreamarchers-logo.svg" alt="DA logo" class="da-logo">
                <h1 class="da-system-title">Time Log</h1>
                
                <form class="da-form">
                    <input type="text" name="username" id="username" placeholder="Username">
                    <input type="password" name="password" id="password" placeholder="Password">
                    <input class="log-btn" type="button" name="login" id="login" value="Login">
                    <input class="log-btn" type="button" name="logout" id="logout" value="Logout">
                </form>
                <p style="text-align:center;">New user? <a id="reg" class="da-reg">Register Here.</a></p>
                <div style="text-align:center;">
                    <span class="vl"></span>
                </div>
                <div id="timer"> </div>
            </div>

            <!-- Body -->
            <div class="col da-content">
                <p style="float:right;">Date: <input type="text" id="datepicker" value="<?php echo date('Y-m-d');?>"></p>
                <div id="time-log">
                    <?php 
                        $user = new user();
                        $curr_date = date('Y-m-d');
                        $user->queryTable($curr_date);
                    ?>
                </div>
                <div class="print-div">
                    <a href="print.php" target="_blank"> 
                        <input style="margin-top:32px;width:180px;float:right;" type="button" name="print_btn" id="print_btn" value="Print">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div>
        <h1>This is sample test</h1>
    </div>

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script style="text/javascript" src="js/triggers.js"></script>
  </body>
</html>