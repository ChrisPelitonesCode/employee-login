<?php
require_once 'class_PDO.php';

class user extends myDB{


public function register(){

	$reg_email = htmlentities($_POST['reg_email']);
	$reg_pass = htmlentities($_POST['reg_pass']);
	$reg_firstname = htmlentities(ucfirst($_POST['reg_firstname']));
	$reg_lastname = htmlentities(ucfirst($_POST['reg_lastname']));

	$birthday = $_POST['year']."-".$_POST['month']."-".$_POST['day'];
	$date_today = date('Y-m-d');
	$sql = "INSERT INTO user (user_title,email,pass,first_name,last_name,birthday,gender,date_registered) 
			   VALUES (?,?,?,?,?,?,?,?)";
	
	$this->connectDB();
	$reg_res=$this->execQuery($sql,array('Member',$reg_email,md5($reg_pass),$reg_firstname,$reg_lastname,$birthday,$_POST['gender'],$date_today));
	$this->closeDB();
	return $reg_res;
}

public function userLogin(){

	$email = $_POST['login_email'];
	$pass = md5($_POST['login_pass']);
	
	$sql = "SELECT iduser,pass,first_name,last_name,user_title,banned_status,account_status FROM user WHERE (email = ? AND pass = ?) AND banned_status NOT LIKE ?";

	$this->connectDB();
	$login_res = $this->queQuery($sql,array($email,$pass,1));
	
	if($login_res){
	$login_res  =  $login_res[0];
	$_SESSION['user_title'] = $login_res['user_title'];
	$_SESSION['user_name'] = $login_res['first_name'].' '.$login_res['last_name'];
	$_SESSION['account_status']=$login_res['account_status'];
	$_SESSION['token']=$login_res['pass'];
		
	if($login_res['account_status']==1){
	
	$sql2="SELECT category.category FROM category
	INNER JOIN group_admin
	ON group_admin.category=category.idcategory
	WHERE group_admin.user_iduser=?
	";
	
	$account_status=$this->queQuery($sql2,array($login_res['iduser']));
		
	if($account_status){
		$account_status=$account_status[0];
		$_SESSION['group_category']=$account_status['category'];
	}
	
	}
		
	$this->closeDB();
	return $login_res['iduser'];
	}else{
	return false;
	}
	
}

public function autoLogin(){

	$sql = "SELECT iduser,first_name,last_name,user_title,banned_status,account_status FROM user WHERE (email = ? AND pass = ?) AND banned_status NOT LIKE ?";

	$this->connectDB();
	$login_res = $this->queQuery($sql,array(strrev($_COOKIE['alfUserEmail']),$_COOKIE['alfUserPass'],1));
	

	if($login_res){
	$login_res  =  $login_res[0];
	$_SESSION['user_title'] = $login_res['user_title'];
	$_SESSION['user_name'] = $login_res['first_name'].' '.$login_res['last_name'];
	$_SESSION['account_status']=$login_res['account_status'];
	
	
	if($login_res['account_status']==1){
	
	$sql2="SELECT category.category FROM category
	INNER JOIN group_admin
	ON group_admin.category=category.idcategory
	WHERE group_admin.user_iduser=?
	";
	
	$account_status=$this->queQuery($sql2,array($login_res['iduser']));
		
	if($account_status){
		$account_status=$account_status[0];
		$_SESSION['group_category']=$account_status['category'];
	}
	
	}
		
	$this->closeDB();
	return $login_res['iduser'];
	}else{
	return false;
	}

}

	
public function fill($category){

$sql="SELECT post.idpost,post.post_title,post.text,post.date_posted,user.iduser,user.user_title,user.pass,user.first_name,user.last_name,user.profile_pic
	FROM post
	INNER JOIN user
	ON post.user_iduser=user.iduser
	INNER JOIN category
	ON post.category=category.idcategory
	WHERE post.announcement=? AND category.category=? ORDER BY post.date_posted DESC";
	
$this->connectDB();
$announcement=$this->queQuery($sql,array(1,$category));
$this->closeDB();

return $announcement;

}

public function queryBirthday(){

$date_today=date('m-d');

$sql="SELECT iduser,first_name,last_name,pass FROM user WHERE birthday LIKE '_____".$date_today."' ORDER BY iduser ASC";

$this->connectDB();
$celebrants=$this->queQuery($sql,array());
$this->closeDB();
return $celebrants;

}

public function queryAllUsers(){

$sql="SELECT iduser,first_name,last_name,pass FROM user";

$this->connectDB();
$users=$this->queQuery($sql,array());
$this->closeDB();

return $users;

}

public function countRecords($table,$condition_set=NULL,$condition=NULL,$condition_set_2=NULL,$condition2=NULL){

if($condition_set!=NULL && $condition_set_2!=NULL)
{
$sql = "SELECT COUNT(*) as total FROM ".$table." WHERE ".$condition_set.$condition." AND ".$condition_set_2.$condition2."";
}
else if($condition_set!=NULL && $condition_set_2==NULL)
{
$sql = "SELECT COUNT(*) as total FROM ".$table." WHERE ".$condition_set.$condition."";
}
else{
$sql = "SELECT COUNT(*) as total FROM ".$table."";
}

$this->connectDB();
if ($res = $this->queryCount($sql)) {

  if ($res->fetchColumn() > 0) {
      
		$res=$this->queryCount($sql);
		return $res->fetchColumn(); 
    } else {
      return 0;
    }
}
$this->closeDB();

}

public function profilePic(){

$sql="SELECT profile_pic FROM user WHERE iduser=?";
$this->connectDB();
$profile_pic=$this->queQuery($sql,array($_SESSION['user_id']));
$this->closeDB();
if($profile_pic){
$profile_pic=$profile_pic[0];
return $profile_pic['profile_pic'];
}else{
$profile_pic=$profile_pic[0];
return $profile_pic['profile_pic'];
}

}

public function getMyInfo($userID){

$sql="SELECT email,pass,first_name,last_name,birthday,gender,location,profile_pic FROM user WHERE iduser=?";
$this->connectDB();
$myInfo=$this->queQuery($sql,array($userID));
$this->closeDB();
return $myInfo;

}


public function updateUserInfo(){

$flag_1=false;
$flag_2=false;
$flag_3=false;

$cond_id=$_SESSION['user_id'];
$set_fname=$_POST['set_fname'];
$set_lname=$_POST['set_lname'];

if($_FILES["file"]["size"] > 0)
{
$set_image=$this->uploadImage();
}
else{
$set_image=$_SESSION['profile_pic'];
unset($_SESSION['profile_pic']);
}

$set_email=$_POST['set_email'];

if($_POST['set_year']=="" || $_POST['set_month']=="" || $_POST['set_day']==""){
	$flag_1=true;
}else{
	$new_birthday=$_POST['set_year'].'-'.$_POST['set_month'].'-'.$_POST['set_day'];
}

if(!isset($new_birthday)){
	$new_birthday=$_SESSION['old_bday'];
	unset($_SESSION['old_bday']);
	$flag_1=false;
}

if($_POST['old_pass']=="" || $_POST['set_r_pass']=="" || $_POST['set_pass']==""){
	$old_pass=$_SESSION['old_pass'];
	$set_r_pass=$_SESSION['old_pass'];
	unset($_SESSION['old_pass']);
	$flag_2=true;
}else if($_SESSION['old_pass']==md5($_POST['old_pass'])){	
	$old_pass=$_POST['old_pass'];
	$set_r_pass=$_POST['set_r_pass'];
}else{
	$flag_3=true;
}

$set_gender=$_POST['set_gender'];
$set_location=$_POST['set_location'];

if($flag_2){
$sql="
	UPDATE user 
    SET email="."'".$set_email."'".",
		pass = CASE WHEN pass="."'".$old_pass."'"." THEN "."'".$set_r_pass."'"." END, 
		first_name="."'".$set_fname."'".", last_name="."'".$set_lname."'".", birthday="."'".$new_birthday."'".",
		gender="."'".$set_gender."'".", location="."'".$set_location."'".", profile_pic="."'".$set_image."'"."
    WHERE iduser=?";
}else if($flag_3){
$sql="";
}else{
$sql="
	UPDATE user 
    SET email="."'".$set_email."'".",
		pass = CASE WHEN pass="."'".md5($old_pass)."'"." THEN "."'".md5($set_r_pass)."'"." END, 
		first_name="."'".$set_fname."'".", last_name="."'".$set_lname."'".", birthday="."'".$new_birthday."'".",
		gender="."'".$set_gender."'".", location="."'".$set_location."'".", profile_pic="."'".$set_image."'"."
    WHERE iduser=?";
}


if($flag_1==true || $flag_3==true || $set_image==false){
return false;
}else{
$this->connectDB();
$updateInfo_res=$this->execQuery($sql,array($cond_id));
$this->closeDB();
$_SESSION['user_name']=$set_fname.' '.$set_lname;
return $updateInfo_res;
}

}

public function uploadImage(){

$allowedExts = array("gif", "jpeg", "jpg", "png");
$temp = explode(".", $_FILES["file"]["name"]);
$extension = end($temp);
$temp_name=$temp[0].date('Y-m-d').$_SESSION['user_id'].'.'.$extension;

if ((($_FILES["file"]["type"] == "image/gif")
|| ($_FILES["file"]["type"] == "image/jpeg")
|| ($_FILES["file"]["type"] == "image/jpg")
|| ($_FILES["file"]["type"] == "image/pjpeg")
|| ($_FILES["file"]["type"] == "image/x-png")
|| ($_FILES["file"]["type"] == "image/png"))
&& in_array($extension, $allowedExts)) {
	if ($_FILES["file"]["error"] > 0) {
    return false;
  } else {
    
      move_uploaded_file($_FILES["file"]["tmp_name"],dirname(dirname(__FILE__))."/img/upload/".$temp_name);
      $location=dirname($_SERVER['PHP_SELF'])."/img/upload/".$temp_name;  	  
	  return $location;
 }
} else {
	var_dump($_FILES["file"]["error"]);
	var_dump($_FILES["file"]["size"]);
	die();
	return false;
}

}

public function markAsRead(){

$sql="
	UPDATE messages SET message_status=? WHERE from_userid=? AND to_userid=?
";

$this->connectDB();
$status_res=$this->execQuery($sql,array(1,$_POST['from_id'],$_POST['my_id']));
$this->closeDB();

}


public function queryAnnouncement(){

$sql="SELECT post.idpost, post.post_title, post.text, post.date_posted, post.category, user.first_name, user.last_name
		FROM post, user
		WHERE post.announcement = 1
		AND post.user_iduser = user.iduser";
	  	  
$this->connectDB();
$announcement_list=$this->queQuery($sql,array());
$this->closeDB();
return $announcement_list;

}

public function queryAdministrators(){


$sql="SELECT user.iduser,user.user_title,user.first_name,user.last_name,category.category 
	FROM user
	INNER JOIN group_admin
	ON user.iduser=group_admin.user_iduser
	INNER JOIN category
	ON category.idcategory=group_admin.category
	WHERE (user_title=? OR user_title=?) AND (iduser NOT LIKE ? AND group_admin.admin_status=?)";

$this->connectDB();
$admin_list=$this->queQuery($sql,array('Administrator','Moderator',$_SESSION['user_id'],1));
$this->closeDB();
return $admin_list;

}

public function querySuperAdminId(){

$sql="SELECT iduser,first_name,last_name FROM user WHERE user_title=? AND iduser NOT LIKE ?";
$this->connectDB();
$super_admin=$this->queQuery($sql,array('Administrator',$_SESSION['user_id']));
$this->closeDB();
return $super_admin;

}


public function queryMyMessages($my_userid,$from_userid){

$sql="SELECT messages.from_userid,messages.to_userid,messages.date_send,messages.message,user.first_name,user.last_name,user.profile_pic
	FROM messages
	INNER JOIN user
	ON messages.from_userid=user.iduser
	WHERE (messages.to_userid=? AND from_userid=?) OR (messages.to_userid=? AND from_userid=?)
	ORDER BY messages.idsend DESC
	";
	
$this->connectDB();
$my_messages=$this->queQuery($sql,array($my_userid,$from_userid,$from_userid,$my_userid));
$this->closeDB();
return $my_messages;

}



public function sendMessage(){

if((isset($_POST['sender_id']) && $_POST['sender_id']!=0) && (isset($_POST['receiver_id']) && $_POST['receiver_id']!=0)){
$date_send = date("Y-m-d H:i:s");
$sql="INSERT INTO messages (from_userid,to_userid,date_send,message) VALUES (?,?,?,?)";

$this->connectDB();
$sending_res=$this->execQuery($sql,array($_POST['sender_id'],$_POST['receiver_id'],$date_send,htmlentities($_POST['message'])));
$this->closeDB();
return $sending_res;
}

}

public function queryReportedUsers(){

$sql="SELECT iduser,first_name,last_name,pass FROM user WHERE EXISTS (SELECT banned.user_iduser FROM banned WHERE banned.user_iduser=user.iduser)";

$this->connectDB();
$reportedUsers=$this->queQuery($sql,array());
$this->closeDB();
return $reportedUsers;

}

public function reportUser(){

$date_reported=date('Y-m-d');
$sql="INSERT INTO banned(user_iduser,date_reported,reason) VALUES (?,?,?)";

$this->connectDB();
$reported_res=$this->execQuery($sql,array($_POST['reported_user'],$date_reported,htmlentities($_POST['reason_of_report'])));
$this->closeDB();
return $reported_res;

}

public function queryUsers(){

	$sql="SELECT iduser,email FROM user u
			WHERE NOT EXISTS
			(
			SELECT  user_iduser 
			FROM    group_admin g
			WHERE   g.user_iduser = u.iduser
			)
			AND (u.user_title!=? AND u.user_title!=?)
			";

	$this->connectDB();
	$users=$this->queQuery($sql,array('Administrator','Moderator'));
	return $users;
	
}

public function checkPage(){

$sql="SELECT category FROM pages WHERE category=?";
$this->connectDB();
$users=$this->queQuery($sql,array($_GET['page']));
return $users;

}

public function getPages(){

$sql="SELECT idcategory,category,icon FROM category WHERE category!='Home'";
$this->connectDB();
$groups=$this->queQuery($sql,array());
$this->closeDB();
return $groups;

}

public function post(){
$date_today=date('Y-m-d');

if($_SESSION['user_title']=='Moderator' || $_SESSION['user_title']=='Administrator'){
	$sql="INSERT INTO post (post_title,text,date_posted,category,status,user_iduser) 
	VALUES (?,?,?,?,?,?)";
	$this->connectDB();
	$post_res=$this->execQuery($sql,array(htmlentities($_POST['discussion_title']),$_POST['discussion_content'],$date_today,$_POST['dicussion_category'],1,$_SESSION['user_id']));
	$this->closeDB();
	return $post_res;
}else{
	$sql="INSERT INTO post (post_title,text,date_posted,category,user_iduser) 
	VALUES (?,?,?,?,?)";

	$this->connectDB();
	$post_res=$this->execQuery($sql,array(htmlentities($_POST['discussion_title']),$_POST['discussion_content'],$date_today,$_POST['dicussion_category'],$_SESSION['user_id']));
	$this->closeDB();
	return $post_res;
}

}

public function getMyDiscussion(){

$sql="SELECT idpost,post_title FROM post WHERE user_iduser=? AND status=?";
$this->connectDB();
$my_discussion=$this->queQuery($sql,array($_SESSION['user_id'],1));
$this->closeDB();
return $my_discussion;

}

public function getPageDiscussion($category,$limit){

$sql="SELECT post.idpost,post.post_title,post.text,post.date_posted,user.iduser,user.user_title,user.first_name,user.last_name,user.pass,user.profile_pic
	FROM post
	INNER JOIN user
	ON post.user_iduser=user.iduser
	WHERE post.category=? AND post.status=? AND post.announcement=? ORDER BY post.date_posted DESC LIMIT ".$limit."";
	
$this->connectDB();
$dicussions=$this->queQuery($sql,array($category,1,0));
$this->closeDB();

return $dicussions;
}

public function getPageName($id){

$sql="SELECT category FROM category WHERE idcategory=?";
$this->connectDB();
$name=$this->queQuery($sql,array($id));
$this->closeDB();

if($name)
{
$name=$name[0];
return $name['category'];
}
}

public function getReplies($post_id){

$sql="SELECT replies.date_reply,replies.message,user.iduser,user.user_title,user.pass,user.first_name,user.last_name
	FROM replies
	INNER JOIN user
	ON replies.user_iduser=user.iduser
	WHERE replies.post_idpost=?
	ORDER BY replies.idmessages DESC";
$this->connectDB();
$replies=$this->queQuery($sql,array($post_id));
$this->closeDB();

return $replies;
}

public function getMainTopic(){

$sql="SELECT post.idpost,post.post_title,post.text,post.date_posted,category.idcategory,category.category,user.iduser,user.user_title,user.pass,user.first_name,user.last_name
FROM post 
INNER JOIN category
ON post.category=category.idcategory
INNER JOIN user 
ON post.user_iduser=user.iduser
WHERE post.idpost=? AND post.status=?";
	
$this->connectDB();
$main_topic=$this->queQuery($sql,array($_GET['id'],1));
$this->closeDB();

return $main_topic;

}

public function replyToDiscussion($post_id,$sender_id,$message){

if((isset($sender_id) && $sender_id!=0) && (isset($post_id) && $post_id!=0)){

$date_reply = date("Y-m-d H:i:s");

$sql="INSERT INTO replies (user_iduser,post_idpost,date_reply,message) VALUES (?,?,?,?)";
	
$this->connectDB();
$reply_topic=$this->execQuery($sql,array($sender_id,$post_id,$date_reply,$message));
$this->closeDB();
return $reply_topic;
}

}

public function getUserInfo(){

if((isset($_GET['id']) && $_GET['id']!="") && (isset($_GET['token']) && $_GET['token']!="")){
$sql="SELECT user_title,first_name,last_name,birthday,gender,location,profile_pic,date_registered FROM user WHERE iduser=? AND pass=?";

$this->connectDB();
$user_info=$this->queQuery($sql,array($_GET['id'],$_GET['token']));
$this->closeDB();
return $user_info;
}
else{
return false;
}

}

public function getUserTopic(){

if(isset($_GET['id']) && $_GET['id']!=""){

$sql="SELECT idpost,post_title FROM post WHERE user_iduser=?";
$this->connectDB();
$user_topic=$this->queQuery($sql,array($_GET['id']));
$this->closeDB();
return $user_topic;

}else{
return false;
}

}

public function newForgotPass($random_pass,$email){

$sql="UPDATE user SET forgot_pass=? WHERE email LIKE ?";
$this->connectDB();
$newForgotPass=$this->execQuery($sql,array($random_pass,$email));
$this->closeDB();

}

public function updatePassword(){

if(isset($_GET['email']) && isset($_GET['newpass'])){

$sql="UPDATE user SET pass=? WHERE forgot_pass LIKE ? AND email LIKE ?";
$this->connectDB();
$newForgotPass=$this->execQuery($sql,array(md5($_GET['newpass']),$_GET['newpass'],$_GET['email']));
$this->closeDB();
return $newForgotPass;
}else{
return false;
}

}

public function queryPost($search){

$sql="SELECT idpost,post_title FROM post WHERE LOWER(post_title) LIKE ?";
$this->connectDB();
$post_title=$this->queQuery($sql,array(strtolower("%".$search."%")));
$this->closeDB();
return $post_title;

}

}