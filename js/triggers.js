$(document).ready(function () {

            function isEmpty(str) {
                return (!str || 0 === str.length);
            }

            //Clock
            function update() {
            $.ajax({
                type: 'POST',
                url: 'datetime.php',
                timeout: 1000,
                success: function(data) {
                    $("#timer").html(data); 
                    window.setTimeout(update, 1000);
                }
                });
                }
            update();

            $('#reg').on('click', function () {
                $('.da-reg-cont').removeClass('hide');
            });
            $('#reg-close').on('click', function () {
                $('.da-reg-cont').addClass('hide');
            });

            //date picker
            $(function(){
                $("#datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function(dateText, inst) { 
                    var curr_date = dateText; 

                    $.post('controller.php', {curr_date}, function(response) {
                    $( "#time-log" ).empty().append(response);
                    });

                }
                });
            });

            //new registration 
            $('#emp_register').on('click', function () {     
                
                var fullname = $('#emp_name').val();
                var username = $('#emp_username').val();
                var password = $('#emp_password').val();
                var registration = 'registration';

                if((isEmpty(fullname)) || (isEmpty(username)) || (isEmpty(password))){
                    
                    alert("Please input all fields(s)");	
                    $("#emp_name").val("");
                    $("#emp_username").val("");
                    $("#emp_password").val("");    

                } else {

                    $.post('controller.php', {registration, fullname, username, password}, function(response) {
                    $( "#time-log" ).empty().append(response);
                    });

                    $("#emp_name").val("");
                    $("#emp_username").val("");
                    $("#emp_password").val("");
                    $('.da-reg-cont').addClass('hide');
                    
                }

            });

            //login 
            $('#login').on('click', function (){        

                var username = $('#username').val();
                var password = $('#password').val();
                var login = "login";

                if((isEmpty(username)) || (isEmpty(password))){
                    
                    alert("Please input all fields(s)");	
                    $("#username").val("");
                    $("#password").val("");    

                } else {

                    //alert(username+":"+password+":"+login);

                    $.post('controller.php', {login, username, password}, function(response) {
                    $( "#time-log" ).empty().append(response);
                    });

                    $("#username").val("");
                    $("#password").val("");   
                }
                
            });

            //logout
            $('#logout').on('click', function (){        

                var username = $('#username').val();
                var password = $('#password').val();
                var logout = "logout";

                if((isEmpty(username)) || (isEmpty(password))){
                    
                    alert("Please input all fields(s)");	
                    $("#username").val("");
                    $("#password").val("");    

                } else {

                    //alert(username+":"+password+":"+login);

                    $.post('controller.php', {logout, username, password}, function(response) {
                    $( "#time-log" ).empty().append(response);
                    });

                    $("#username").val("");
                    $("#password").val("");   
                }
                
            });

        });