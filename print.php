<?php require_once 'controller.php'; ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap-grid.css">
    <link rel="stylesheet" href="css/jquery-ui.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/style.css">

    <title>Employee Login System</title>
  </head>
  <body>

  	<div class="print-div">
  		<input class="log-btn" type="button" name="logout" onclick="printContent('print-content')" value="Print">
    </div>
  	
  	<div id="print-content">	
	            <!-- Body -->
	    <div class="col da-content">
	    	<img src="img/dreamarchers-logo.svg" alt="DA logo" class="da-logo">
	        <p style="float:right;">Date: <input type="text" id="datepicker" value="<?php echo date('Y-m-d');?>"></p>
	        <div id="time-log">
	            <?php 
	                $user = new user();
	                $curr_date = date('Y-m-d');
	                $user->queryTable($curr_date);
	            ?>
	        </div>
	    </div>
  	</div>


    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script style="text/javascript" src="js/triggers.js"></script>
    <script>
    	function printContent (el) {
    		var restorepage = document.body.innerHTML;
    		var printcontent = document.getElementById(el).innerHTML;
    		document.body.innerHTML = printcontent;
    		window.print();
    		document.body.innerHTML = restorepage;
    	}
    </script>
  </body>
</html>