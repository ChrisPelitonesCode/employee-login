
	<?php

	class myDB {
	
		var $host = 'localhost';
		var $dbse = 'employeelogin';
		var $user = 'root';
		var $pass = '';
		var $dbhandler = NULL;
		var $res = NULL;
		
		public function connectDB(){
			try{
				$this->dbhandler = new PDO('mysql:host=' . $this->host .';port=3306;dbname='.$this->dbse, $this->user, $this->pass);
				$this->setErrModes();
				return true;
			}catch(PDOException $ex){
				echo "Error: " . $ex->getMessage();
				file_put_contents("PDOErrors.txt", $ex->getMessage(),FILE_APPEND);
				return false;
				die();
			}
		}
		
		private function setErrModes(){
			try{
				$this->dbhandler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->dbhandler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
				$this->dbhandler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);				
				return $this->dbhandler;
			}catch(PDOException $ex){
				echo "Error: " . $ex->getMessage();
				file_put_contents("PDOErrors.txt", $ex->getMessage(),FILE_APPEND);
				die();
			}
		}
		
		protected function queryCount($querystring){
			try{				
				$this->res = $this->dbhandler->query($querystring);
				return $this->res;
			}catch(PDOException $ex){
				echo "Error: " . $ex->getMessage();
				file_put_contents("PDOErrors.txt", $ex->getMessage(),FILE_APPEND);
				die();
			}
		}
			
		protected function queQuery($querystring,$data){		
			$sth = $this->dbhandler->prepare($querystring);
			$sth->execute($data);
			return $sth->fetchAll();	
		}

		protected function fetchQuery($querystring){				
			$sth = $this->dbhandler->query($querystring);
			return $sth->fetchAll();
		}

		protected function execQuery($querystring,$data){
			$sth = $this->dbhandler->prepare($querystring);
			return @$sth->execute($data);
		}
		
		public function closeDB(){
			try{
				$this->dbhandler = NULL;
				return true;
			}catch(PDOException $ex){
				echo "Error: " . $ex->getMessage();
				file_put_contents("PDOErrors.txt", $ex->getMessage(),FILE_APPEND);
				die(); 
			}
		}
	}
	
	?>
	